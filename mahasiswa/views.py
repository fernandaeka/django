from django.shortcuts import render
from . import models
# Create your views here.

dataMhs = models.Mahasiswa.objects.all()

context = { 'judul':'Web Mahasiswa',
            'pageName':'Halaman Tampil Mahasiswa',
            'kontributor':'Fernanda Eka Novitasari',
            'mahasiswa':dataMhs}

def index(request):
    return render(request, 'mahasiswa/index.html',context)